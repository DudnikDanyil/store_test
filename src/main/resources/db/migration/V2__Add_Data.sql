INSERT INTO users (id, username, first_name, last_name, email, password, status)
VALUES (3, 'user1', 'user1', 'user1', 'email@example.com', '$2a$10$8nBAMfziop8Crze9H7b8RuafvAGt/wBDk9m9AfbRbGT7POJ6I3.ai', 'ACTIVE');

INSERT INTO users (id, username, first_name, last_name, email, password, status)
VALUES (4, 'user2', 'user2', 'user2', 'email@example.com', '$2a$10$8nBAMfziop8Crze9H7b8RuafvAGt/wBDk9m9AfbRbGT7POJ6I3.ai', 'ACTIVE');


INSERT INTO roles (id, name, status)
VALUES (1, 'ROLE_USER', 'ACTIVE');

INSERT INTO roles (id, name, status)
VALUES (2, 'ROLE_ADMIN', 'ACTIVE');

INSERT INTO user_roles (user_id, role_id)
VALUES (3, 1);

INSERT INTO user_roles (user_id, role_id)
VALUES (4, 2);

INSERT INTO public.products (product_name, quantity, price, characteristic, date_added, status)
VALUES ('IPhone 12', 30, 1000, 'Innovative technologies', '2023-10-15 14:00:00', 'ACTIVE');
