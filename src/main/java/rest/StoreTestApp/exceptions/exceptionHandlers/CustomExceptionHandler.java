package rest.StoreTestApp.exceptions.exceptionHandlers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import rest.StoreTestApp.exceptions.ProductAlreadyExistsException;
import rest.StoreTestApp.exceptions.SuperAdminDeletionException;
import rest.StoreTestApp.exceptions.UserAlreadyExistsException;
import rest.StoreTestApp.model.ApiError;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
@Slf4j
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(SuperAdminDeletionException.class)
    public ResponseEntity<Object> handleSuperAdminDeletionException(SuperAdminDeletionException ex, WebRequest request) {
        String message = ex.getMessage();
        ApiError apiError = new ApiError(HttpStatus.FORBIDDEN, message, request.getDescription(false));
        logger.error("Handled SuperAdminDeletionException: " + message);
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<Object> handleUserAlreadyExistsException(UserAlreadyExistsException ex, WebRequest request) {
        String message = ex.getMessage();
        ApiError apiError = new ApiError(HttpStatus.CONFLICT, message, request.getDescription(false));
        logger.error("Handled UserAlreadyExistsException: " + message);
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException ex, WebRequest request) {
        String message = ex.getMessage();
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, message, request.getDescription(false));
        logger.error("Handled EntityNotFoundException: " + message);
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    @ExceptionHandler(ProductAlreadyExistsException.class)
    public ResponseEntity<Object> handleProductAlreadyExistsException(ProductAlreadyExistsException ex, WebRequest request) {
        String message = ex.getMessage();
        ApiError apiError = new ApiError(HttpStatus.CONFLICT, message, request.getDescription(false));
        logger.error("Handled ProductAlreadyExistsException: " + message);
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }
}
