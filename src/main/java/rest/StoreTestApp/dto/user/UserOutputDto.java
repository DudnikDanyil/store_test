package rest.StoreTestApp.dto.user;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class UserOutputDto {

    @Schema(description = "User's id", example = "1")
    private int id;
    @Schema(description = "User's username", example = "user1")
    private String username;
    @Schema(description = "User's first name", example = "user1")
    private String firstName;
    @Schema(description = "User's last name", example = "user1")
    private String lastName;
    @Schema(description = "User's email", example = "email@example.com")
    private String email;

}
