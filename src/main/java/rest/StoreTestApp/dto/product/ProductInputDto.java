package rest.StoreTestApp.dto.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductInputDto {

    @NotNull
    @Size(min = 5, max = 100)
    private String productName;

    @NotNull
    private int quantity;

    @NotNull
    private BigDecimal price;

    @NotNull
    @Size(min = 10, max = 1000)
    private String characteristic;
}
