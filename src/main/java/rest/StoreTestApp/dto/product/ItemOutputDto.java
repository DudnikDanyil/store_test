package rest.StoreTestApp.dto.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemOutputDto {

    private int id;
    private int idProduct;
    private String productName;
    private int quantity;
    private BigDecimal price;
    private String characteristic;
    private int customUserId;
    private String status;

}
