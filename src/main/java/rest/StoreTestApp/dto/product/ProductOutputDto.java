package rest.StoreTestApp.dto.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductOutputDto {

    private int id;
    private String productName;
    private int quantity;
    private BigDecimal price;
    private String characteristic;

}
