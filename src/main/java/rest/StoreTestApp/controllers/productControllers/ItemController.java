package rest.StoreTestApp.controllers.productControllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.StoreTestApp.dto.product.ItemOutputDto;
import rest.StoreTestApp.services.ItemService;
import rest.StoreTestApp.services.mapper.MapperService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/product/order")
public class ItemController {

    private final ItemService itemService;
    private final MapperService modelMapper;

    @PostMapping("/add")
    ResponseEntity<ItemOutputDto> orderProduct(@RequestParam("product_id") int product_id,
                                               @RequestParam("quantity") int quantity){

        ItemOutputDto itemOutputDto = modelMapper.convertToItemOutputDto(
                itemService.orderProduct(product_id, quantity));

        return ResponseEntity.ok(itemOutputDto);
    }

    @GetMapping("/allByUser")
    ResponseEntity<List<ItemOutputDto>> getAllItemByUser(){


        List <ItemOutputDto> itemOutputListDto = modelMapper.convertToItemOutputDtoList(
                itemService.getAllItemsByUser());

        return ResponseEntity.ok(itemOutputListDto);
    }

    @PostMapping("/pay")
    ResponseEntity<List<ItemOutputDto>> payForOrder(){

        List <ItemOutputDto> itemOutputListDto = modelMapper.convertToItemOutputDtoList(
                itemService.payForOrder());

        return ResponseEntity.ok(itemOutputListDto);
    }
}
