package rest.StoreTestApp.controllers.productControllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.StoreTestApp.dto.product.ProductInputDto;
import rest.StoreTestApp.dto.product.ProductOutputDto;
import rest.StoreTestApp.services.ProductService;
import rest.StoreTestApp.services.mapper.MapperService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/product")
public class ProductController {
    private final ProductService productService;
    private final MapperService modelMapper;

    @GetMapping("/all")
    ResponseEntity<List<ProductOutputDto>> getAllProducts(){


        List<ProductOutputDto> productList = modelMapper.convertToProductOutputDtoList(
                productService.getAllProducts());

        return ResponseEntity.ok(productList);
    }

    @PostMapping("/add")
    ResponseEntity<ProductOutputDto> addNewProduct(@Valid @RequestBody ProductInputDto productInputDto){

         ProductOutputDto productOutputDto = modelMapper.convertToProductOutputDto(
                 productService.addProduct(modelMapper.convertToProduct(productInputDto)));

        return ResponseEntity.ok(productOutputDto);
    }


}
