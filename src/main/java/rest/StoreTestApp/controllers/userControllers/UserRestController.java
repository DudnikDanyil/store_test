package rest.StoreTestApp.controllers.userControllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.StoreTestApp.dto.user.UserInputDto;
import rest.StoreTestApp.dto.user.UserOutputDto;
import rest.StoreTestApp.services.mapper.MapperService;
import rest.StoreTestApp.services.UserService;

import javax.validation.Valid;


@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/user/")
public class UserRestController {
    private final UserService userService;
    private final MapperService mapperService;

    @PostMapping(value = "/registr")
    public ResponseEntity<UserOutputDto> registrationUser(@Valid @RequestBody UserInputDto userInputDto) {

        UserOutputDto userOutputDto = mapperService.convertToUserDto(userService
                .register(mapperService.convertToUser(userInputDto)));

        return ResponseEntity.ok(userOutputDto);
    }

}
