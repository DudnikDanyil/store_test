package rest.StoreTestApp.controllers.userControllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.StoreTestApp.dto.user.AdminUserDto;
import rest.StoreTestApp.model.user.User;
import rest.StoreTestApp.services.mapper.MapperService;
import rest.StoreTestApp.services.UserService;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/admin/")
public class AdminRestController {

    private final UserService userService;

    private final MapperService mapperService;

    @GetMapping(value = "user/{id}")
    public ResponseEntity<AdminUserDto> getUserById(@PathVariable(name = "id") int id) {
        User user = userService.findById(id);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        AdminUserDto result = mapperService.convertToAdminUserDto(user);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity<Void> deletingUser(@RequestParam("id") Integer id) {

        userService.delete(id);

        return ResponseEntity.ok().build();
    }
}
