package rest.StoreTestApp.services;

import org.springframework.security.access.prepost.PreAuthorize;
import rest.StoreTestApp.model.user.User;

import java.util.List;

public interface UserService {

    User register(User user);

    List<User> getAll();

    User findByUsername(String username);
    @PreAuthorize("hasRole('USER')")
    User findById(Integer id);

    void delete(Integer id);
}
