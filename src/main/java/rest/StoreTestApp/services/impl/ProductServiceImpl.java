package rest.StoreTestApp.services.impl;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rest.StoreTestApp.exceptions.ProductAlreadyExistsException;
import rest.StoreTestApp.model.emuns.Status;
import rest.StoreTestApp.model.product.Product;
import rest.StoreTestApp.repositories.ProductRepository;
import rest.StoreTestApp.services.ProductService;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public List<Product> getAllProducts() {

        return productRepository.findAll();
    }

    @Transactional
    @Override
    public Product addProduct(Product product) {

        checkIfProductExists(product);

        product.setStatus(Status.valueOf("ACTIVE"));
        log.info("Adding a new product: {}", product.getProductName());
        return productRepository.save(product);
    }

    private void checkIfProductExists(Product product) {
        if (productRepository.existsProductByProductName(product.getProductName())) {
            throw new ProductAlreadyExistsException("Product with name " + product.getProductName() + " already exists");
        }
    }

}
