package rest.StoreTestApp.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rest.StoreTestApp.model.emuns.StatusOrder;
import rest.StoreTestApp.model.product.Item;
import rest.StoreTestApp.model.product.Product;
import rest.StoreTestApp.repositories.ItemRepository;
import rest.StoreTestApp.repositories.ProductRepository;
import rest.StoreTestApp.security.jwt.JwtUser;
import rest.StoreTestApp.services.ItemService;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ItemServiceImpl implements ItemService {

    private final ProductRepository productRepository;
    private final ItemRepository itemRepository;

    @Transactional
    @Override
    public Item orderProduct(int idProduct, int quantity) {
        Optional<Product> existingProduct = productRepository.findProductById(idProduct);

        if (existingProduct.isPresent()) {
            int availableQuantity = existingProduct.get().getQuantity();

            if (availableQuantity >= quantity) {

                Item existingItem = itemRepository.findItemByIdProductAndUserIdAndStatus(idProduct,
                        getUserIdFromCurrentSession(), StatusOrder.NOT_PAID);

                if (existingItem != null) {

                    int newQuantity = existingItem.getQuantity() + quantity;
                    existingItem.setQuantity(newQuantity);
                    return itemRepository.save(existingItem);
                } else {

                    Item item = buildItemFromProduct(existingProduct.get(), quantity);

                    log.info("Ordering a product with ID: {}, Quantity: {}", idProduct, quantity);
                    return itemRepository.save(item);
                }
            }
            throw new EntityNotFoundException("Not enough quantity available for product with id " + idProduct);
        }
        throw new EntityNotFoundException("Product with id " + idProduct + " not found");
    }

    @Transactional
    @Override
    public List<Item> payForOrder() {

        Optional<List<Item>> itemsListOptional = itemRepository.findAllByUserIdAndStatus(getUserIdFromCurrentSession(),
                StatusOrder.NOT_PAID);

        if (itemsListOptional.isPresent()) {

            List<Item> itemsList = itemsListOptional.get();
            itemsList.stream()
                    .forEach(item -> item.setStatus(StatusOrder.PAID));

            itemRepository.saveAll(itemsList);
            updateProductQuantities(itemsList);

            log.info("Paying for orders by user with ID: {}", getUserIdFromCurrentSession());
            return itemsList;
        }

        throw new EntityNotFoundException("User with id " + getUserIdFromCurrentSession() + " has no orders in the cart yet");
    }

    @Override
    public List<Item> getAllItemsByUser() {

        log.info("Getting all items for the current user with ID: {}", getUserIdFromCurrentSession());
        return itemRepository.getAllByUserId(getUserIdFromCurrentSession());
    }


    private void updateProductQuantities(List<Item> itemsList) {
        for (Item item : itemsList) {

            Optional<Product> existingProduct = productRepository.findProductById(item.getIdProduct());

            if (existingProduct.isPresent()) {
                Product product = existingProduct.get();
                int newQuantity = product.getQuantity() - item.getQuantity();
                product.setQuantity(newQuantity);
                productRepository.save(product);
            }
        }
    }

    private Item buildItemFromProduct(Product product, int quantity) {
        return Item.builder()
                .idProduct(product.getId())
                .productName(product.getProductName())
                .quantity(quantity)
                .price(product.getPrice())
                .characteristic(product.getCharacteristic())
                .userId(getUserIdFromCurrentSession())
                .dateAdded(product.getDateAdded())
                .status(StatusOrder.NOT_PAID)
                .build();
    }


    private int getUserIdFromCurrentSession() {
        JwtUser jwtUser = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return jwtUser.getId();
    }
}
