package rest.StoreTestApp.services;

import org.springframework.security.access.prepost.PreAuthorize;
import rest.StoreTestApp.model.product.Item;

import java.util.List;

public interface ItemService {

    @PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
    Item orderProduct(int product_id, int quantity);

    @PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
    List<Item> payForOrder();

    @PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
    List<Item> getAllItemsByUser();
}
