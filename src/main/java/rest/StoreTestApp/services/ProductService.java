package rest.StoreTestApp.services;

import org.springframework.security.access.prepost.PreAuthorize;
import rest.StoreTestApp.model.product.Product;

import java.util.List;

public interface ProductService {

    @PreAuthorize("hasRole('ADMIN') OR hasRole('USER')")
    List<Product> getAllProducts();

    @PreAuthorize("hasRole('ADMIN')")
    Product addProduct(Product product);

}
