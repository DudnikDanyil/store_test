package rest.StoreTestApp.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import rest.StoreTestApp.model.emuns.StatusOrder;
import rest.StoreTestApp.repositories.ItemRepository;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class ScheduledTaskService {

    private final ItemRepository itemRepository;

    @Scheduled(cron = "${job.delete-unpaid-order}")
    @Transactional
    public void deleteUnpaidItemsScheduled() {

        LocalDateTime tenMinutesAgo = LocalDateTime.now().minusMinutes(10);
        itemRepository.deleteByStatusAndDateRegistrationBefore(StatusOrder.NOT_PAID, tenMinutesAgo);
    }
}
