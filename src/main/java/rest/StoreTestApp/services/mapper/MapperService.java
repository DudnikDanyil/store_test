package rest.StoreTestApp.services.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rest.StoreTestApp.dto.product.ItemOutputDto;
import rest.StoreTestApp.dto.product.ProductInputDto;
import rest.StoreTestApp.dto.product.ProductOutputDto;
import rest.StoreTestApp.dto.user.AdminUserDto;
import rest.StoreTestApp.dto.user.UserInputDto;
import rest.StoreTestApp.dto.user.UserOutputDto;
import rest.StoreTestApp.model.product.Item;
import rest.StoreTestApp.model.product.Product;
import rest.StoreTestApp.model.user.User;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MapperService {

    private final ModelMapper modelMapper;

    @Autowired
    public MapperService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public User convertToUser(UserInputDto userInputDto) {
        return modelMapper.map(userInputDto, User.class);
    }

    public UserOutputDto convertToUserDto(User user) {
        return modelMapper.map(user, UserOutputDto.class);
    }

    public AdminUserDto convertToAdminUserDto(User user) {
        return modelMapper.map(user, AdminUserDto.class);
    }

    public ProductInputDto convertToProductInputDto(Product product){
        return modelMapper.map(product, ProductInputDto.class);
    }

    public ProductOutputDto convertToProductOutputDto(Product product){
        return modelMapper.map(product, ProductOutputDto.class);
    }

    public Product convertToProduct(ProductInputDto productInputDto){
        return modelMapper.map(productInputDto, Product.class);
    }

    public ItemOutputDto convertToItemOutputDto(Item item){
        ItemOutputDto itemOutputDto = modelMapper.map(item, ItemOutputDto.class);
        itemOutputDto.setCustomUserId(item.getUserId());
        return itemOutputDto;
    }

    public List<ProductOutputDto> convertToProductOutputDtoList(List<Product> products) {
        return products.stream()
                .map(product -> modelMapper.map(product, ProductOutputDto.class))
                .collect(Collectors.toList());
    }

    public List<ItemOutputDto> convertToItemOutputDtoList(List<Item> items) {
        return items.stream()
                .map(item -> modelMapper.map(item, ItemOutputDto.class))
                .collect(Collectors.toList());
    }

}
