package rest.StoreTestApp.model.product;

import lombok.*;
import rest.StoreTestApp.model.emuns.StatusOrder;
import rest.StoreTestApp.model.user.User;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "items")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "id_product")
    private int idProduct;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "characteristic")
    private String characteristic;

    @Column(name = "date_added")
    private LocalDateTime dateAdded;

    @Column(name = "user_id")
    private int userId;

    @Column(name = "date_registration")
    private LocalDateTime dateRegistration;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private StatusOrder status;

    @PrePersist
    public void prePersist() {
        dateRegistration = LocalDateTime.now();
    }


    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

}
