package rest.StoreTestApp.model.product;

import lombok.*;
import rest.StoreTestApp.model.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "products")
public class Product extends BaseEntity {

    @Column(name = "product_name")
    private String productName;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "characteristic")
    private String characteristic;

    @Column(name = "date_added")
    private LocalDateTime dateAdded;

    @PrePersist
    public void prePersist() {
        dateAdded= LocalDateTime.now();
    }

}
