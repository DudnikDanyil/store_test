package rest.StoreTestApp.model.emuns;

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}
