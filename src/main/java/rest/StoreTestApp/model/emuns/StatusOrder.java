package rest.StoreTestApp.model.emuns;

public enum StatusOrder {

    PAID, NOT_PAID
}
