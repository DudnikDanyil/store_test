package rest.StoreTestApp.model.user;

import lombok.Data;
import lombok.ToString;
import rest.StoreTestApp.model.BaseEntity;
import rest.StoreTestApp.model.product.Item;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "users")
@Data
@ToString(exclude = "items")
public class User extends BaseEntity {

    @Column(name = "username")
    private String username;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private List<Role> roles;

    @OneToMany(mappedBy = "user")
    private List<Item> items;
}
