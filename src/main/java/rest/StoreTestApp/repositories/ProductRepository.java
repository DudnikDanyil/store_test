package rest.StoreTestApp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rest.StoreTestApp.model.product.Product;

import javax.swing.text.html.Option;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

        boolean existsProductByProductName(String name);
        Optional<Product> findProductById(int id);
}
