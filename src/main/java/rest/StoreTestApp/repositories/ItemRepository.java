package rest.StoreTestApp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import rest.StoreTestApp.model.emuns.StatusOrder;
import rest.StoreTestApp.model.product.Item;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ItemRepository extends JpaRepository<Item, Integer> {

    Item findItemByIdProductAndUserIdAndStatus(int product_id, int user_id, StatusOrder status);
    Optional<List<Item>> findAllByUserIdAndStatus(int userId, StatusOrder status);
    void deleteByStatusAndDateRegistrationBefore(StatusOrder status, LocalDateTime date);

    List<Item> getAllByUserId(int userId);
}
