
DROP TABLE IF EXISTS users cascade;
DROP TABLE IF EXISTS roles cascade;
DROP TABLE IF EXISTS user_roles cascade;
DROP TABLE IF EXISTS products cascade;
DROP TABLE IF EXISTS items cascade;



CREATE TABLE IF NOT EXISTS users (
                                            id SERIAL PRIMARY KEY,
                                            username VARCHAR(255) NOT NULL,
                                            first_name VARCHAR(255) NOT NULL,
                                            last_name VARCHAR(255) NOT NULL,
                                            email VARCHAR(255) NOT NULL,
                                            password VARCHAR(255) NOT NULL,
                                            status VARCHAR(25) DEFAULT 'ACTIVE'
    );

CREATE TABLE IF NOT EXISTS roles (
                                            id SERIAL PRIMARY KEY,
                                            name VARCHAR(255),
                                            status VARCHAR(25) DEFAULT 'ACTIVE'
    );


CREATE TABLE IF NOT EXISTS user_roles (
                                            user_id INT REFERENCES users(id),
                                            role_id INT REFERENCES roles(id)
    );

CREATE TABLE IF NOT EXISTS products (
                                               id SERIAL PRIMARY KEY,
                                               product_name VARCHAR(255) NOT NULL,
                                               quantity INT NOT NULL,
                                               price INT NOT NULL,
                                               characteristic VARCHAR(1000) NOT NULL,
                                               date_added TIMESTAMP NOT NULL,
                                               status VARCHAR(25) DEFAULT 'ACTIVE' NOT NULL
    );

CREATE TABLE IF NOT EXISTS items (
                                            id SERIAL PRIMARY KEY,
                                            product_name VARCHAR(255) NOT NULL,
                                            id_product INT NOT NULL,
                                            quantity INT NOT NULL,
                                            price INT NOT NULL,
                                            characteristic VARCHAR(1000) NOT NULL,
                                            date_added TIMESTAMP NOT NULL,
                                            status VARCHAR(25) DEFAULT 'ACTIVE',
                                            date_registration TIMESTAMP,
                                            user_id INT,
                                            FOREIGN KEY (user_id) REFERENCES users(id)
    );