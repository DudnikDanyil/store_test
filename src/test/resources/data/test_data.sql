INSERT INTO users (username, first_name, last_name, email, password, status)
VALUES ('user1', 'user1', 'user1', 'email1@example.com', '$2a$10$8nBAMfziop8Crze9H7b8RuafvAGt/wBDk9m9AfbRbGT7POJ6I3.ai', 'ACTIVE');

INSERT INTO users (username, first_name, last_name, email, password, status)
VALUES ('user2', 'user2', 'user2', 'email2@example.com', '$2a$10$8nBAMfziop8Crze9H7b8RuafvAGt/wBDk9m9AfbRbGT7POJ6I3.ai', 'ACTIVE');

INSERT INTO users (username, first_name, last_name, email, password, status)
VALUES ('user3', 'user3', 'user3', 'email3@example.com', '$2a$10$8nBAMfziop8Crze9H7b8RuafvAGt/wBDk9m9AfbRbGT7POJ6I3.ai', 'ACTIVE');


INSERT INTO roles (id, name, status)
VALUES (1, 'ROLE_USER', 'ACTIVE');

INSERT INTO roles (id, name, status)
VALUES (2, 'ROLE_NOT_USER', 'ACTIVE');

INSERT INTO roles (id, name, status)
VALUES (3, 'ROLE_ADMIN', 'ACTIVE');

INSERT INTO user_roles (user_id, role_id)
VALUES (1, 1);

INSERT INTO user_roles (user_id, role_id)
VALUES (2, 2);

INSERT INTO user_roles (user_id, role_id)
VALUES (3, 3);


INSERT INTO public.products (product_name, quantity, price, characteristic, date_added, status)
VALUES ('iPhone 11', 40, 3000, 'Innovative technologies.', '2023-10-15 14:00:00.000', 'ACTIVE');

INSERT INTO public.products (product_name, quantity, price, characteristic, date_added, status)
VALUES ('Samsung Galaxy Flip5', 30, 2000, 'Well, it''s not an iPhone, but it''s still a great smartphone.', '2023-10-15 14:00:00.000', 'ACTIVE');

INSERT INTO public.products (product_name, quantity, price, characteristic, date_added, status)
VALUES ('Xiaomi Redmi 12', 20, 2000, 'Well, it can make calls, you know.', '2023-10-15 14:00:00.000', 'ACTIVE');