package rest.StoreTestApp.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import rest.StoreTestApp.controllers.data.ProductControllerData;
import rest.StoreTestApp.dto.product.ItemOutputDto;
import rest.StoreTestApp.dto.product.ProductOutputDto;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Sql(scripts = {"classpath:/data/test_schema.sql", "classpath:/data/test_data.sql"})
@ActiveProfiles("test")
public class ProductControllerTest extends ProductControllerData {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void getAllProducts_Test() throws Exception {

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(GET_ALL_PRODUCT)
                        .header(HEADER_KEY, HEADER_VALUE + generateTokenUserRole()))
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();

        List<ProductOutputDto> products = objectMapper.readValue(content, new TypeReference<>() {
        });

        assertFalse(products.isEmpty());
        assertEquals(3, products.size());
    }

    @Test
    void getAllProducts_Unauthorized_Test() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get(GET_ALL_PRODUCT)
                        .header(HEADER_KEY, HEADER_VALUE + generateTokenNotUserRole()))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    void addNewProduct_Test() throws Exception {

        MvcResult result = mockMvc.perform(post(ADD_NEW_PRODUCT)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generatedProductInputDto()))
                        .header(HEADER_KEY, HEADER_VALUE + generateTokenAdminRole()))
                .andExpect(status().isOk())
                .andReturn();

        ProductOutputDto expectedProductOutput = generatedProductOutputDto();

        String content = result.getResponse().getContentAsString();
        ProductOutputDto actualProductOutput = objectMapper.readValue(content, ProductOutputDto.class);

        assertEquals(expectedProductOutput, actualProductOutput);
    }

    @Test
    void addNewProduct_Unauthorized_Test() throws Exception {

        mockMvc.perform(post(ADD_NEW_PRODUCT)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generatedProductInputDto()))
                        .header(HEADER_KEY, HEADER_VALUE + generateTokenNotUserRole()))
                .andExpect(status().isForbidden())
                .andReturn();
    }


    @Test
    void orderProduct_Test() throws Exception {

        MvcResult orderResult = mockMvc.perform(post(ADD_NEW_ORDER)
                        .param("product_id", String.valueOf(1))
                        .param("quantity", String.valueOf(8))
                        .header(HEADER_KEY, HEADER_VALUE + generateTokenUserRole()))
                .andExpect(status().isOk())
                .andReturn();

        ItemOutputDto expectedItemOutput = generatedItemOutputDto(1, 8);

        String content = orderResult.getResponse().getContentAsString();
        ItemOutputDto actualItemOutput = objectMapper.readValue(content, ItemOutputDto.class);

        assertEquals(expectedItemOutput, actualItemOutput);
    }

    @Test
    void orderProduct_Unauthorized_Test() throws Exception {
        mockMvc.perform(post(ADD_NEW_ORDER)
                        .param("product_id", String.valueOf(1))
                        .param("quantity", String.valueOf(8))
                        .header(HEADER_KEY, HEADER_VALUE + generateTokenNotUserRole()))
                .andExpect(status().isForbidden())
                .andReturn();
    }


    @Test
    void payForOrder_Test() throws Exception {

        mockMvc.perform(post(ADD_NEW_ORDER)
                        .param("product_id", String.valueOf(1))
                        .param("quantity", String.valueOf(8))
                        .header(HEADER_KEY, HEADER_VALUE + generateTokenUserRole()))
                .andExpect(status().isOk())
                .andReturn();

        MvcResult payResult = mockMvc.perform(post(PEY_ORDER)
                        .header(HEADER_KEY, HEADER_VALUE + generateTokenUserRole()))
                .andExpect(status().isOk())
                .andReturn();

        String payContent = payResult.getResponse().getContentAsString();
        List<ItemOutputDto> paidItems = objectMapper.readValue(payContent, new TypeReference<List<ItemOutputDto>>() {
        });

        for (ItemOutputDto item : paidItems) {
            assertEquals("PAID", item.getStatus());
        }
    }


    @Test
    void payForOrder_Unauthorized_Test() throws Exception {
        mockMvc.perform(post(PEY_ORDER)
                        .header(HEADER_KEY, HEADER_VALUE + generateTokenNotUserRole()))
                .andExpect(status().isForbidden())
                .andReturn();
    }
}
