package rest.StoreTestApp.controllers.data;

import org.springframework.beans.factory.annotation.Autowired;
import rest.StoreTestApp.dto.product.ItemOutputDto;
import rest.StoreTestApp.dto.product.ProductInputDto;
import rest.StoreTestApp.dto.product.ProductOutputDto;
import rest.StoreTestApp.model.user.Role;
import rest.StoreTestApp.security.jwt.JwtTokenProvider;

import java.util.ArrayList;
import java.util.List;

public class ProductControllerData {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    public static final String GET_ALL_PRODUCT = "/api/product/all";

    public static final String ADD_NEW_PRODUCT = "/api/product/add";

    public static final String ADD_NEW_ORDER = "/api/product/order/add";

    public static final String PEY_ORDER = "/api/product/order/pay";
    public static final String HEADER_KEY = "Authorization";

    public static final String HEADER_VALUE = "Bearer_";

    protected ProductInputDto generatedProductInputDto() {
        ProductInputDto productInputDto = new ProductInputDto();
        productInputDto.setProductName("iPhone 12");
        productInputDto.setQuantity(40);
        productInputDto.setPrice(3000);
        productInputDto.setCharacteristic("Innovative technologies.");

        return productInputDto;
    }

    protected ProductOutputDto generatedProductOutputDto() {
        ProductOutputDto productOutputDto = new ProductOutputDto();
        productOutputDto.setId(4);
        productOutputDto.setProductName("iPhone 12");
        productOutputDto.setQuantity(40);
        productOutputDto.setPrice(3000);
        productOutputDto.setCharacteristic("Innovative technologies.");
        return productOutputDto;
    }

    protected ItemOutputDto generatedItemOutputDto(int productId, int quantity) {
        ItemOutputDto itemOutputDto = new ItemOutputDto();
        itemOutputDto.setId(1);
        itemOutputDto.setIdProduct(productId);
        itemOutputDto.setProductName("iPhone 11");
        itemOutputDto.setQuantity(quantity);
        itemOutputDto.setPrice(3000);
        itemOutputDto.setCharacteristic("Innovative technologies.");
        itemOutputDto.setCustomUserId(1);
        itemOutputDto.setStatus("NOT_PAID");
        return itemOutputDto;
    }

    protected String generateTokenUserRole() {
        List<Role> roles = new ArrayList<>();
        Role role = new Role();
        role.setName("ROLE_USER");
        roles.add(role);
        return jwtTokenProvider.createToken("user1", roles, 1);
    }

    protected String generateTokenAdminRole() {
        List<Role> roles = new ArrayList<>();
        Role role = new Role();
        role.setName("ROLE_ADMIN");
        roles.add(role);
        return jwtTokenProvider.createToken("user3", roles, 3);
    }

    protected String generateTokenNotUserRole() {
        List<Role> roles = new ArrayList<>();
        Role role = new Role();
        role.setName("ROLE_NOT_USER");
        roles.add(role);
        return jwtTokenProvider.createToken("user2", roles, 2);
    }
}
